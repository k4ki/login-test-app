package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


@NamedQueries({
    @NamedQuery(name = User.FIND_BY_EMAIL, query = "select u from User u where u.email = :email"), 
    @NamedQuery(name = User.COUNT_EMAIL, query = "select Count(u.email) from User u where u.email = :email"), 
    @NamedQuery(name = User.FIND_ALL, query = "select u from User u"), 
    @NamedQuery(name = User.USER_COUNT, query = "select count(u) from User u"), 
})
@SuppressWarnings("serial")
@XmlRootElement
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "EMAIL"),name = "USER")
@Entity
public class User implements Serializable {
    public static final String FIND_BY_EMAIL = "User.findByEmail";
    public static final String COUNT_EMAIL = "User.countEmail";
    public static final String FIND_ALL = "User.findAll";
    public static final String USER_COUNT = "User.userCount";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID")
    private Long id;

    @NotNull(message="{email.required}")
    @Pattern(regexp = "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9]"
            + "(?:[A-Za-z0-9-]*[A-Za-z0-9])?",
            message = "{invalid.email}")
    @Column(name="EMAIL")
    private String email;

    @XmlTransient
    @NotNull(message="{password.required}")
    @Column(name="PASSWORD")
    private String password;

    @NotNull(message="First Name Required")
    @Column(name="FIRSTNAME")
    private String firstName;
    
    @NotNull(message="Last Name Required")
    @Column(name="LASTNAME")
    private String lastName;
    
    @ManyToMany(fetch=FetchType.EAGER)
    private List<Role> roles;

    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public Long getId() {
        return id;
    }
    
    public List<Role> getRoles() {
        return roles;
    }
    
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
    /**
     * Check to see if the User is has Admin role
     * 
     * @return true if admin
     */
    @XmlTransient
    public boolean isAdmin() {
        for ( Role role: roles ) {
            if ( role.getRole().equals("ADMIN")) return true;
        }
        return false;
    }

}
