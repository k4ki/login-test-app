-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: baluDB
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB-1~xenial
LOCK TABLES `ROLE` WRITE;
INSERT INTO `ROLE` VALUES (1,'USER'),(2,'ADMIN');
UNLOCK TABLES;

LOCK TABLES `USER` WRITE;
INSERT INTO `USER` VALUES (1,'davkun107@live.de','David','Kunschke','WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U='),(2,'fanny@fanny.de','Fanny','Brauer','WZRHGrsBESr8wYFZ9sx0tPURuZgG2lmzyvWpwXPKz8U=');
UNLOCK TABLES;


LOCK TABLES `USER_ROLE` WRITE;
INSERT INTO `USER_ROLE` VALUES (1,1),(1,2),(2,1);
UNLOCK TABLES;

